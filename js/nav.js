const createNav = () => {
    let nav = document.querySelector('.navbar');

    nav.innerHTML = `
        <div class="nav">
            <img src="photo/brasseria.jpeg" class="brand-logo" alt="">
            <div class="nav-items">
                <div class="search">
                    <input type="text" class="search-box" placeholder="search food,product">
                    <button class="search-btn">search</button>
                </div>
                <a href="#"><img src="photo/user.png" alt=""></a><div class="two columns u-pull-right">
          <ul>
            <li class="submenu">
              <img src="photo/cart.png" id="img-carrito">
              <div id="carrito">
                <table id="lista-carrito" class="u-full-width">
                  <thead>
                    <tr>
                      <th>Nume</th>
                      <th>Cantitate</th>
                      <th>Pret</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
                <a href="#" id="vaciar-carrito" class="button u-full-width">Golire cos</a>
              </div>
            </li>
          </ul>
        </div>
        
            </div>
        </div>
        <ul class="links-container">
            <li class="link-item"><a href="#" class="link">Sandwich-uri</a></li>
            <li class="link-item"><a href="#" class="link">Salate</a></li>
            <li class="link-item"><a href="#" class="link">Specialitati Carne</a></li>
            <li class="link-item"><a href="#" class="link">Deserturi</a></li>
            <li class="link-item"><a href="#" class="link">Platouri Evenimente</a></li>
        </ul>
        
        
    
    `;
}

createNav();