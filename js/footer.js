const createFooter = () => {
    let footer = document.querySelector('footer');

    footer.innerHTML = `
    <div class="footer-content">
            <img src="photo/brasseria.jpeg" class="logo" alt="">
            <div class="footer-ul-container">
                <ul class="category">
                    <li class="category-title">Starter</li>
                    <li><a href="#" class="footer-link">Bruschete ton</a> </li>
                    <li><a href="#" class="footer-link">Bruschete avocado</a> </li>
                    <li><a href="#" class="footer-link">Bruschete rosii</a> </li>
                    <li><a href="#" class="footer-link">Hummus cu rodie</a> </li>
                    <li><a href="#" class="footer-link">Crema de anghinare</a> </li>
                    <li><a href="#" class="footer-link">Salata vinete</a> </li>
                </ul>
                <ul class="category">
                    <li class="category-title">Fel Principal</li>
                    <li><a href="#" class="footer-link">Sandwich 109</a> </li>
                    <li><a href="#" class="footer-link">Sandwich avocado</a> </li>
                    <li><a href="#" class="footer-link">Salata 109</a> </li>
                    <li><a href="#" class="footer-link">Salata Caezar</a> </li>
                    <li><a href="#" class="footer-link">Piept de pui cu sos gorgonzola</a> </li>
                    <li><a href="#" class="footer-link">Ficatei de pui in vin</a> </li>
                    <li><a href="#" class="footer-link">Cotlet de porc la gratar</a> </li>
                    <li><a href="#" class="footer-link">Antricot de vita</a> </li>
                </ul>
            </div>
        </div>

        <p class="footer-title">Despre noi</p>
            <p class="info">Cauți gustul mâncării “ca la mama acasă”?, prăjituri 100% naturale?, o ciorbă caldă pentru masa de prânz sau doar o cafea de savurat pe terasă dimineața?
            Brasseria 109 îți oferă toate aceste oportunități!</p>
        <p class="info">Suport Email - brasseria109@gmail.com</p>
        <p class="info">Telefon - 0763099181</p>
        <div class="footer-social-container">
            <div>
                <a href="#" class="social-link">Termene si Servicii</a>
                <a href="#" class="social-link">Privacy Page</a>
            </div>
            <div>
                <a href="https://www.facebook.com/brasserie109" class="social-link">Facebook</a>
                <a href="https://www.instagram.com/brasserie_109/" class="social-link">Instagram</a>
            </div>
        </div>
        <p class="footer-credit">Mancare ca la mama acasa!</p>
    
    `;
}

createFooter();